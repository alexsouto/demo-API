package com.example.demo;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.sql.Timestamp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo.model.ErrorDTO;
import com.example.demo.model.PriceResult;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private TestRestTemplate template;

	// Test 1: petición a las 10:00 del día 14 del producto 35455 para la brand 1
	// (ZARA)
	@Test
	public void test1() {
		PriceResult priceResult = (PriceResult) getPricesByParams(Timestamp.valueOf("2020-06-14 10:00:00"), 1, 35455);
		// Como cada precio es diferente no necesitamos comprobar los demás valores para
		// saber que es el resultado esperado

		// El resultado debe ser 35.50EUR
		assertThat(priceResult.getPrice() == "35.50EUR");
	}

	// Test 2: petición a las 16:00 del día 14 del producto 35455 para la brand 1
	// (ZARA)
	@Test
	public void test2() {
		PriceResult priceResult = (PriceResult) getPricesByParams(Timestamp.valueOf("2020-06-14 16:00:00"), 1, 35455);

		// El resultado debe ser 24.45EUR
		assertThat(priceResult.getPrice() == "24.45EUR");
	}

	// Test 3: petición a las 21:00 del día 14 del producto 35455 para la brand 1
	// (ZARA))
	@Test
	public void test3() {
		PriceResult priceResult = (PriceResult) getPricesByParams(Timestamp.valueOf("2020-06-14 21:00:00"), 1, 35455);

		// El resultado debe ser 35.50EUR
		assertThat(priceResult.getPrice() == "35.50EUR");
	}

	// Test 4: petición a las 10:00 del día 15 del producto 35455 para la brand 1
	// (ZARA)
	@Test
	public void test4() {
		PriceResult priceResult = (PriceResult) getPricesByParams(Timestamp.valueOf("2020-06-15 10:00:00"), 1, 35455);

		// El resultado debe ser 30.50EUR
		assertThat(priceResult.getPrice() == "30.50EUR");

	}

	// Test 5: petición a las 21:00 del día 16 del producto 35455 para la brand 1
	// (ZARA)
	@Test
	public void test5() {
		PriceResult priceResult = (PriceResult) getPricesByParams(Timestamp.valueOf("2020-06-15 21:00:00"), 1, 35455);

		// El resultado debe ser 38.95EUR
		assertThat(priceResult.getPrice() == "38.95EUR");
	}

	@Test
	public void BadRequestDateTest() {
		ResponseEntity<?> response = template
				.getForEntity("/prices/date/" + "aaa" + "/product/" + "35455" + "/brand/" + "1", ErrorDTO.class);

		ErrorDTO errorDTO = (ErrorDTO) response.getBody();
		// Comprobamos que da un error de bad request
		assertThat(errorDTO.getStatus().equals(HttpStatus.BAD_REQUEST));
	}

	@Test
	public void BadRequestBrandTest() {
		ResponseEntity<String> response = template.getForEntity(
				"/prices/date/" + "2020-06-15 21:00:00" + "/product/" + "35455" + "/brand/" + "-2", String.class);

		// Comprobamos que da un error de bad request
		assertThat(response.getStatusCode().equals(HttpStatus.BAD_REQUEST));
	}

	@Test
	public void NotFoundTest() {
		ResponseEntity<String> response = template.getForEntity(
				"/prices/date/" + "2020-06-15 21:00:00" + "/product/" + "35458" + "/brand/" + "1", String.class);

		// Comprobamos que da un error de not found
		assertThat(response.getStatusCode().equals(HttpStatus.NOT_FOUND));
	}

	//////////////////////////////// FUNCIONES//////////////////////////////////////////////
	public PriceResult getPricesByParams(Timestamp date, Integer brand_id, Integer product_id) {

		ResponseEntity<PriceResult> response = template.getForEntity(
				"/prices/date/" + date + "/product/" + product_id + "/brand/" + brand_id, PriceResult.class);

		return response.getBody();
	}

}
