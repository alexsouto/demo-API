package com.example.demo.mappers;

import java.text.DecimalFormat;

import com.example.demo.model.Price;
import com.example.demo.model.PriceResult;

public class PriceToPriceResultMapperImpl implements PriceToPriceResultMapper {

	private static final DecimalFormat df = new DecimalFormat("0.00");

	@Override
	public PriceResult priceToPriceResult(Price source) {
		if (source == null) {
			return null;
		}
		PriceResult priceResult = new PriceResult();
		priceResult.setBrand_id(source.getBrand_id());
		priceResult.setStart_date(source.getStart_date());
		priceResult.setEnd_date(source.getEnd_date());
		priceResult.setPrice_list(source.getPrice_list());
		priceResult.setProduct_id(source.getProduct_id());
		priceResult.setPrice(df.format(source.getPrice()) + source.getCurr());

		return priceResult;
	}

}
