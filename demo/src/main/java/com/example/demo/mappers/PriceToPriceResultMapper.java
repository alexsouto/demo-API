package com.example.demo.mappers;

import org.mapstruct.Mapper;

import com.example.demo.model.Price;
import com.example.demo.model.PriceResult;

@Mapper
public interface PriceToPriceResultMapper {
	PriceResult priceToPriceResult(Price source);
}
