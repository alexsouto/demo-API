package com.example.demo.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.ErrorDTO;
import com.example.demo.model.Price;
import com.example.demo.model.PriceResult;
import com.example.demo.service.PriceService;
import com.example.demo.validators.ValidPositiveInteger;
import com.example.demo.validators.ValidTimestamp;

@Validated
@RestController
public class Controller {

	@Autowired
	PriceService priceService;

	@RequestMapping("/")
	public String hello() {
		return "Este servicio devuelve el precio para un producto, momento temporal y marca dados.\nUtilice el endpoint /prices/date/{date}/product/{product_id}/brand/{brand_id}.";
	}

	@GetMapping("/prices")
	private List<Price> getAllPrices() {
		List<Price> prices = priceService.getAllPrices();
		return prices;
	}

	@GetMapping("/prices/date/{date}/product/{product_id}/brand/{brand_id}")
	@ResponseBody
	// Como con el @DateTimeFormat(pattern=yyyy-MM-dd HH:mm:ss) no conseguí que
	// funcionase, creé mi propia anotación para validar el timestamp
	// El @Min(value = 0) @Digits(fraction = 0, integer = 1) tampoco me funcionó
	// para los enteros positivos, así que también creé una propia
	private ResponseEntity<?> getPrices(@Valid @PathVariable("date") @NotNull @ValidTimestamp Timestamp date,
			@Valid @PathVariable("product_id") @NotNull @ValidPositiveInteger Long product_id,
			@Valid @PathVariable("brand_id") @NotNull @ValidPositiveInteger Long brand_id) {

		PriceResult priceResult = priceService.getPricesByParams(date, product_id, brand_id);
		if (priceResult != null)
			return new ResponseEntity<PriceResult>(priceResult, HttpStatus.OK);

		else {
			ErrorDTO errorDTO = new ErrorDTO();
			errorDTO.addError(
					"No existe precio para los valores solicitados. O no existe el producto, o no existe la marca, o no hay precio para ese producto de esa marca en esa fecha.");
			errorDTO.setStatus(HttpStatus.NOT_FOUND);
			return new ResponseEntity<ErrorDTO>(errorDTO, HttpStatus.NOT_FOUND);

		}

	}

}