package com.example.demo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ErrorDTO {
	private Date timestamp;
	private HttpStatus status;
	private List<String> errors = new ArrayList<>();

	public ErrorDTO() {
		this.timestamp = new Date();
	}

	public void addError(String message) {
		this.errors.add(message);
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus badRequest) {
		this.status = badRequest;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}