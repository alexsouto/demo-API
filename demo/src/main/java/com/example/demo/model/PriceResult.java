package com.example.demo.model;

import java.sql.Timestamp;

public class PriceResult {

	Integer product_id;
	Integer brand_id;
	Integer price_list;
	Timestamp start_date;
	Timestamp end_date;
	String price;

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public Integer getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(Integer brand_id) {
		this.brand_id = brand_id;
	}

	public Integer getPrice_list() {
		return price_list;
	}

	public void setPrice_list(Integer price_list) {
		this.price_list = price_list;
	}

	public Timestamp getStart_date() {
		return start_date;
	}

	public void setStart_date(Timestamp start_date) {
		this.start_date = start_date;
	}

	public Timestamp getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Timestamp end_date) {
		this.end_date = end_date;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

}