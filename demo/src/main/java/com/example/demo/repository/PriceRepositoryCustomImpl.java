package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.model.Price;

import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

public class PriceRepositoryCustomImpl implements PriceRepositoryCustom {

	@Autowired
	private EntityManager entityManager;

	public Price findPricesByParams(Timestamp date, Long product_id, Long brand_id) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Price> criteriaQuery = criteriaBuilder.createQuery(Price.class);

		Root<Price> price = criteriaQuery.from(Price.class);
		List<Predicate> predicates = new ArrayList<>();

		// Filtramos poir fechas
		predicates.add(criteriaBuilder.lessThanOrEqualTo(price.get("start_date"), date));
		predicates.add(criteriaBuilder.greaterThanOrEqualTo(price.get("end_date"), date));

		// Filtramos por product id
		predicates.add(criteriaBuilder.equal(price.get("product_id"), product_id));

		// Filtramos por brand_id
		predicates.add(criteriaBuilder.equal(price.get("brand_id"), brand_id));

		criteriaQuery.where(predicates.toArray(new Predicate[0]));

		List<Price> results = entityManager.createQuery(criteriaQuery).getResultList();

		// En caso de que haya varios los ordenamos por prioridad
		Collections.sort(results, (o1, o2) -> o2.getPriority() - o1.getPriority());

		// Si no hay ningun resultado devolvemos null, si lo hay devolvemos el más
		// prioritario
		return results.size() > 0 ? results.get(0) : null;
	}
}
