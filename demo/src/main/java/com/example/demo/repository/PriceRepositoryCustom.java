package com.example.demo.repository;

import java.sql.Timestamp;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Price;

@Repository
public interface PriceRepositoryCustom {
	public Price findPricesByParams(Timestamp date, Long product_id, Long brand_id);
}
