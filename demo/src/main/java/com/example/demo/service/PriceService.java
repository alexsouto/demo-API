package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.mappers.PriceToPriceResultMapper;
import com.example.demo.model.Price;
import com.example.demo.model.PriceResult;
import com.example.demo.repository.PriceRepository;

@Service
public class PriceService {

	@Autowired
	PriceRepository priceRepository;

	private PriceToPriceResultMapper mapper = Mappers.getMapper(PriceToPriceResultMapper.class);

	public List<Price> getAllPrices() {
		List<Price> prices = priceRepository.findAll();
		return prices;
	}

	public PriceResult getPricesByParams(Timestamp date, Long product_id, Long brand_id) {
		Price price = priceRepository.findPricesByParams(date, product_id, brand_id);

		PriceResult priceResult = mapper.priceToPriceResult(price);
		return priceResult;

	}

}