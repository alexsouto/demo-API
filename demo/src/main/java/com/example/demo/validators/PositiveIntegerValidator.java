package com.example.demo.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PositiveIntegerValidator implements ConstraintValidator<ValidPositiveInteger, Long> {

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		if (value > 0 && value % 1 == 0)
			return true;
		else
			return false;
	}

}