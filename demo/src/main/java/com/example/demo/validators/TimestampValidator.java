package com.example.demo.validators;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TimestampValidator implements ConstraintValidator<ValidTimestamp, Timestamp> {

	@Override
	public boolean isValid(Timestamp value, ConstraintValidatorContext context) {

		SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			format.parse(value.toString());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
