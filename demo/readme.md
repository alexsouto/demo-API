***Developer test***

Este proyecto es una aplicación de ejemplo desarrollada con Spring Boot que proporciona un endpoint para consultar el precio de diferentes artículos en una fecha y hora dadas. 
Utiliza una base de datos H2 que se inicializa al arrancar la aplicación.



***Tecnologías:***
- Java 17
- Spring Boot 3.1.1
- Maven 
- H2 BD



***Los requisitos son los siguientes:***

En la base de datos de comercio electrónico de la compañía disponemos de la tabla PRICES que refleja el precio final (pvp) y la tarifa que aplica a un producto de una cadena entre unas fechas determinadas. A continuación se muestra un ejemplo de la tabla con los campos relevantes:


|BRAND_ID |      START_DATE     |       END_DATE      | PRICE_LIST | PRODUCT_ID | PRIORITY | PRICE | CURR |
| ------- | ------------------- | ------------------- | ---------- | ---------- | -------- | ----- | ---- |
|    1    | 2020-06-14-00.00.00 | 2020-12-31-23.59.59 |      1     |    35455   |     0    | 35.50 |  EUR |
|    1    | 2020-06-14-15.00.00 | 2020-06-14-18.30.00 |      2     |    35455   |     1    | 25.45 |  EUR |
|    1    | 2020-06-15-00.00.00 | 2020-06-15-11.00.00 |      3     |    35455   |     1    | 30.50 |  EUR |
|    1    | 2020-06-15-16.00.00 | 2020-12-31-23.59.59 |      4     |    35455   |     1    | 38.95 |  EUR |


Campos: 
 
BRAND_ID: foreign key de la cadena del grupo (1 = ZARA).

START_DATE , END_DATE: rango de fechas en el que aplica el precio tarifa indicado.

PRICE_LIST: Identificador de la tarifa de precios aplicable.

PRODUCT_ID: Identificador código de producto.

PRIORITY: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rago de fechas se aplica la de mayor prioridad (mayor valor numérico).

PRICE: precio final de venta.

CURR: iso de la moneda.

Se pide:
 
Construir una aplicación/servicio en SpringBoot que provea una end point rest de consulta  tal que:
 
Acepte como parámetros de entrada: fecha de aplicación, identificador de producto, identificador de cadena.
Devuelva como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar.
 
Se debe utilizar una base de datos en memoria (tipo h2) e inicializar con los datos del ejemplo, (se pueden cambiar el nombre de los campos y añadir otros nuevos si se quiere, elegir el tipo de dato que se considere adecuado para los mismos).
              
Desarrollar unos test al endpoint rest que  validen las siguientes peticiones al servicio con los datos del ejemplo:
                                                                                       
-          Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)
-          Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)
-          Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)
-          Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)
-          Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)

***Qué se ha hecho:****
Para llevar a cabo esta tarea se ha creado un endpoint en un controlador que recibiendo los parámetros indicados proporciona el resultado requerido. Esto es, recibiendo una fecha, id de producto e id de marca devuelve un precio para dicho articulo en esa fecha, así como los demás datos que se piden en la respuesta: El id de producto, id de cadena, tarifa y fechas de aplicación. 

Este controlador llama a su vez a un servicio al que solicita dicha información. Tras validar previamente que los datos introducidos cumplan el formato requerido. (Para los id se comprueba que sean enteros positivos y para la fecha que cumpla el formato establecido "yyyy-MM-dd HH:mm:ss"). Estas validaciones se realizan empleando anotaciones del hibernate-validator. También se crearon anotaciones propias en el paquete validators, donde se pueden definir reglas más especificas.

Los errores de validación se gestionan con un handlerException personalizado que crea un objeto ErrorDTO y lo devuelve en caso de que se produzca alguno. 

La base de datos es una base de datos dinámica H2 que se crea al arrancar y que carga la información con un script data.sql. Si se desea que se guarde la base de datos en lugar de crearla y borrarla cada vez está indicado el cómo en el archivo de propiedades application.properties. 

Para la query a base de datos se ha creado un customRepository que se ha añadido a las operaciones básicas que extienden del repositorio JPA. 

Al tener diferentes campos el objeto en base de datos y la información que se solicita, se han creado dos objetos diferentes (Price y PriceResult) y se mapea el primero al segundo usando MapStruct.

Al contener ya la propia especificación tests que comprueban que los resultados sean los esperados no se han realizado más tests de funcionalidad. Si se han añadido tests para comprobar que se devuelva el error esperado si no existe un resultado para los datos introducidos o si se detecta que los parametros son incorrectos. 

-          Test 6 BadRequestDateTest: Se envia una cadena de letras en lugar de una fecha y se comprueba que se devuelve un error 400 BAD REQUEST.
-          Test 7 BadRequestBrandTest: Se envia un numero negativo en el identificador de la marca y se comprueba que se devuelve un error 400 BAD REQUEST.
-          Test 8 NotFoundTest: Se pasan unos parámetros correctos pero para un producto inexistente por lo que se comprueba que se devuelve un 404 NOT FOUND al no encontrar ningún resultado.

Para acceder al método se debe emplear esta url:
localhost:8080/prices/date/{date}/product/{product_id}/brand/{brand_id}

También he considerado oportuno dejar en localhost:8080/ una pequeña descripción del servicio.
A su vez está también disponible localhost:8080/prices, que devuelve todas las entradas de precios que existen. 

***Contribuciones***
Las contribuciones son bienvenidas. Si deseas mejorar este proyecto, sigue los pasos a continuación:

Realiza un fork del proyecto.
Crea una nueva rama (git checkout -b feature/nueva-caracteristica).
Realiza los cambios necesarios y realiza los commits (git commit -am 'Agrega una nueva característica').
Sube los cambios a tu repositorio fork (git push origin feature/nueva-caracteristica).
Abre una solicitud de extracción en el repositorio original.

***Contacto***
Si tienes alguna pregunta o sugerencia, no dudes en contactarme en alexsoutoares@gmail.com.


